<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('datereceived');
            $table->string('productcode');
            $table->integer('productcategory_id');
            $table->string('barcode');
            $table->string('unit');
            $table->string('unitprice');
            $table->string('payamount');
            $table->string('quantity');
            $table->integer('supplier_id');
            $table->string('reorderlimit');
            $table->text('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
